# Advanced Git Workshop
Lab 03: Finding the Guilty

---

# Tasks

 - Inspecting who was the last one in modified a file
 
 - Inspecting who introduced a bug

---

## Preparations

 - Clone the repository:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab3
$ cd lab3
```

 - There is a bug in the index.html file, let's find the guilty (Line 58)

```
<h1 class="text-uppercase mb-0" style="background-color:#2c3e50;color:white">BUG</h1>
```

&nbsp;
<img alt="Image 3.1" src="Images/3.1.png"  width="700" border="1">
&nbsp;
&nbsp;

---

## Inspecting who was the last one in modified a file

 - Let's check who were the last people to modify the file:
 
```
$ git blame index.html
```

 - The output is very long, let's look for something more specific:
 
```
$ git blame -L 55,65 index.html
```

 - Seems like Moises is the guilty, so let's retrieve his email to notify him:
 
```
$ git blame -e -L 58,58 index.html
```

 - But we are really sure that his is the guilty? After all, the command only indicates who was the last to modify the line

&nbsp;

---

## Inspecting who introduced the bug

 - Let's check who create the line of the bug:
 
```
$ git log -S "BUG" --pretty=format:'%h %an %ad %s'
```

 - It seems that it was Daniela who introduced the error, so what did Moses do?
 
```
$ git diff 5f372c5 d5a9a01f
```

 - Moises only modified the error that Daniela introduced, she is the real guilty!
  
---

# Cleanup

 - Remove the repository used during the lab:
 
```
$ cd ..
$ rm -rf lab3
```

 